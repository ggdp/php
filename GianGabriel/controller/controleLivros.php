<?php
    include '../model/crudLivros.php';
    $opcao = $_POST["opcao"];
    switch ($opcao) {
        case 'Cadastrar':
            $titulo = $_POST["titulo"];
            $autor = $_POST["autor"];
            $preco = $_POST["preco"];
            cadastrarLivros($titulo, $autor, $preco);
            header("Location: cadastrarLivros.php");
        break;
        case 'Alterar':
            $codigo = $_POST["codigo"];
            $titulo = $_POST["titulo"];
            $autor = $_POST["autor"];
            $preco = $_POST["preco"];
            alterarLivros($codigo, $titulo, $autor, $preco);
            header("Location: mostrarLivros.php");
        break;
        case 'Excluir':
            $codigo = $_POST["codigo"];
            excluirLivros($codigo);
            header("Location: mostrarLivros.php");
        break;            
    }
?>